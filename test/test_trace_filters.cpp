//
// File: test_trace_filters.cpp
// Created by: Olivier Langella
// Created on: 28/04/2019
//
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// make test ARGS="-V -I 19,19"

#include <QDebug>
#include <QString>
#include <iostream>
#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/processing/filters/filtermorpho.h>
#include <pappsomspp/processing/filters/filtertandemremovec13.h>
#include "common.h"
#include "config.h"

using namespace pappso;
using namespace std;

int
main()
{

  cout << endl << "..:: test trace filters ::.." << endl;
  //   OboPsiMod test;

  MassSpectrum spectrum_simple =
    readMgf(QString(CMAKE_SOURCE_DIR)
              .append("/test/data/peaklist_15046_simple_xt.mgf"));
  //.applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);
  cout << endl << "..:: FilterGreatestY ::.." << endl;

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
  MassSpectrum most_intense(spectrum_simple);
  most_intense.filter(FilterGreatestY(10));

  if(most_intense.size() != 10)
    {
      cerr << "most_intense.size() != 10 " << most_intense.size() << endl;
      return 1;
    }

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
  most_intense = spectrum_simple;
  most_intense.massSpectrumFilter(
    pappso::MassSpectrumFilterGreatestItensities(10));

  if(most_intense.size() != 10)
    {
      cerr << "most_intense.size() != 10 " << most_intense.size() << endl;
      return 1;
    }
  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
  most_intense =
    MassSpectrum(spectrum_simple)
      .massSpectrumFilter(pappso::MassSpectrumFilterGreatestItensities(1000));

  if(most_intense.size() != 100)
    {
      cerr << "most_intense.size() != 100 " << most_intense.size() << endl;
      return 1;
    }
  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
  most_intense = MassSpectrum(spectrum_simple).filter(FilterGreatestY(100));

  if(most_intense.size() != 100)
    {
      cerr << "most_intense.size() != 100 " << most_intense.size() << endl;
      return 1;
    }
  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
  // 3.018830
  cout << endl << "..:: resample filters ::.." << endl;
  Trace deca_suite({DataPoint(0, 1),
                    DataPoint(1, 2),
                    DataPoint(2, 3),
                    DataPoint(3, 4),
                    DataPoint(4, 3),
                    DataPoint(5, 2),
                    DataPoint(6, 1),
                    DataPoint(7, 1),
                    DataPoint(8, 2),
                    DataPoint(9, 3),
                    DataPoint(10, 4),
                    DataPoint(11, 5),
                    DataPoint(12, 6)});
  Trace deca_keep({DataPoint(9, 3), DataPoint(10, 4), DataPoint(11, 5)});

  Trace deca_remove({DataPoint(0, 1),
                     DataPoint(1, 2),
                     DataPoint(2, 3),
                     DataPoint(3, 4),
                     DataPoint(4, 3),
                     DataPoint(5, 2),
                     DataPoint(6, 1),
                     DataPoint(7, 1),
                     DataPoint(8, 2),
                     DataPoint(12, 6)});
  Trace deca_remove2({DataPoint(0, 1),
                      DataPoint(1, 2),
                      DataPoint(2, 3),
                      DataPoint(3, 4),
                      DataPoint(4, 3),
                      DataPoint(5, 2),
                      DataPoint(6, 1),
                      DataPoint(7, 1),
                      DataPoint(8, 2)});
  Trace deca_smaller({DataPoint(0, 1), DataPoint(1, 2), DataPoint(2, 3)});

  Trace deca_greater({DataPoint(10, 4), DataPoint(11, 5), DataPoint(12, 6)});
  Trace deca(deca_suite);
  deca.filter(FilterResampleRemoveXRange(9, 11.5));
  if(deca != deca_remove)
    {
      cerr << "deca != deca_remove " << deca.size() << endl;
      cerr << deca.toString().toStdString().c_str() << endl;
      return 1;
    }
  deca = deca_suite;
  deca.filter(FilterResampleRemoveXRange(9, 12));
  if(deca != deca_remove2)
    {
      cerr << "deca != deca_remove2 " << deca.size() << endl;
      cerr << deca.toString().toStdString().c_str() << endl;
      return 1;
    }

  deca = deca_suite;
  deca.filter(FilterResampleKeepXRange(9, 11));
  if(deca != deca_keep)
    {
      cerr << "deca != deca_keep " << deca.size() << endl;
      cerr << deca.toString().toStdString().c_str() << endl;
      return 1;
    }


  deca = deca_suite;
  deca.filter(FilterResampleKeepSmaller(3));
  if(deca != deca_smaller)
    {
      cerr << "deca != deca_smaller " << deca.size() << endl;
      cerr << deca.toString().toStdString().c_str() << endl;
      return 1;
    }

  deca = deca_suite;
  deca.filter(FilterResampleKeepGreater(9));
  if(deca != deca_greater)
    {
      cerr << "deca != deca_greater " << deca.size() << endl;
      cerr << deca.toString().toStdString().c_str() << endl;
      return 1;
    }
  cout << endl << "..:: morpho filters ::.." << endl;

  deca = deca_suite;
  deca.filter(FilterMorphoMedian(1));
  Trace deca_median({DataPoint(0, 2),
                     DataPoint(1, 2),
                     DataPoint(2, 3),
                     DataPoint(3, 3),
                     DataPoint(4, 3),
                     DataPoint(5, 2),
                     DataPoint(6, 1),
                     DataPoint(7, 1),
                     DataPoint(8, 2),
                     DataPoint(9, 3),
                     DataPoint(10, 4),
                     DataPoint(11, 5),
                     DataPoint(12, 6)});
  if(deca.size() != deca_median.size())
    {
      cerr << "deca.size() != deca_median.size() " << deca.size() << endl;
      cerr << deca.toString().toStdString().c_str() << endl;
      cerr << "origin" << endl
           << deca_median.toString().toStdString().c_str() << endl;
      return 1;
    }
  cout << "deca.size() == deca_median.size() " << deca.size() << endl;
  if(deca != deca_median)
    {
      cerr << "deca != deca_median " << deca.size() << endl;
      cerr << deca.toString().toStdString().c_str() << endl;
      cerr << "origin" << endl
           << deca_median.toString().toStdString().c_str() << endl;
      return 1;
    }

  cout << endl << "..:: FilterTandemRemoveC13 ::.." << endl;
  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
  MassSpectrum remove_c13 =
    MassSpectrum(spectrum_simple).massSpectrumFilter(FilterTandemRemoveC13());

  if(remove_c13.size() != 97)
    {
      cerr << "remove_c13.size() != 97 " << remove_c13.size() << endl;
      return 1;
    }
  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
  return 0;
}
