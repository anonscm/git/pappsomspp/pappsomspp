//
// File: test_timsframe.cpp
// Created by: Olivier Langella
// Created on: 12/4/2020
//
/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// make test ARGS="-V -I 22,22"

#include <QDebug>
#include <QString>
#include <iostream>
#include <pappsomspp/vendors/tims/timsframebase.h>
#include <pappsomspp/mzrange.h>

using namespace pappso;
using namespace std;

int
main()
{

  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  cout << endl << "..:: test TIMS frame init ::.." << endl;
  //   OboPsiMod test;

  TimsFrameBase frame(1, 671);

  double temperature_correction =
    77.0 * (25.4933665127396 - 25.59978165276) +
    (-3.7) * (26.2222755503081 - 27.6311281556638);
  temperature_correction = (double)1.0 + (temperature_correction / 1.0e6);


  frame.setMzCalibration(temperature_correction,
                         0.2,
                         24864.0,
                         313.577620892277,
                         157424.07710945,
                         0.000338743021989553,
                         0.0);
  frame.setTimsCalibration(2,
                           1,
                           670,
                           207.775676931964,
                           59.2526676368822,
                           33.0,
                           1,
                           0.0209889001473149,
                           131.440113097798,
                           12.9712317295887,
                           2558.71692505931);
  frame.setTime(2402.64305686123);
  frame.setMsMsType(0);

  MzRange mz_range(1200.0001, PrecisionFactory::getPpmInstance(10));
  quint32 index = frame.getRawIndexFromMz(mz_range.getMz());
  qDebug() << "index=" << index;
  double tof = frame.getTofFromIndex(index);
  qDebug() << "tof=" << tof;
  double mz = frame.getMzFromTof(tof);
  qDebug() << "mz=" << QString::number(mz, 'g', 10);

  if(!mz_range.contains(mz))
    {

      cerr << "conversion from raw index to mz ERROR "
           << mz_range.toString().toStdString() << " does not contain "
           << QString::number(mz, 'g', 10).toStdString() << endl;
      return 1;
    }

  // 313792
  mz = frame.getMzFromTof(frame.getTofFromIndex((quint32)313793));
  qDebug() << " 313793 => mz=" << QString::number(mz, 'g', 10);
  mz = frame.getMzFromTof(frame.getTofFromIndex((quint32)313792));
  qDebug() << " 313792 => mz=" << QString::number(mz, 'g', 10);
  mz = frame.getMzFromTof(frame.getTofFromIndex((quint32)313791));
  qDebug() << " 313791 => mz=" << QString::number(mz, 'g', 10);

  return 0;
}
