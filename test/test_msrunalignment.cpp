
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// make test ARGS="-V -I 20,20"
// cmake .. -DCMAKE_BUILD_TYPE=Debug  -DMAKE_TEST=1  -DUSEPAPPSOTREE=1


#include <iostream>
//#include <odsstream/tsvreader.h>
#include <odsstream/odsdocreader.h>
#include <odsstream/odsexception.h>
#include <odsstream/odsdocwriter.h>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msrun/alignment/msrunretentiontime.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <QDebug>
#include <QtCore>
#include <QFile>
#include "config.h"
//#include "common.h"

using namespace std;


void
writeTrace(const QString &filename, const pappso::Trace &trace)
{
  QFile file(filename);
  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() "
           << QFileInfo(filename).absoluteFilePath();
  // file.open(QIODevice::WriteOnly);
  OdsDocWriter writer(&file);

  for(auto &data_point : trace)
    {
      writer.writeCell(data_point.x);
      writer.writeCell(data_point.y);
      writer.writeLine();
    }
  writer.close();
}

class CustomHandler : public OdsDocHandlerInterface
{
  public:
  CustomHandler(pappso::MsRunRetentionTime<QString> &rt1,
                pappso::MsRunRetentionTime<QString> &rt2)
    : m_rt1(rt1), m_rt2(rt2)
  {
    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() ";
  }
  /**
   * callback that indicates the begining of a data sheet. Override it in
   * order to retrieve information about the current data sheet.
   *
   */
  virtual void startSheet(const QString &sheet_name) override{};

  /**
   * callback that indicates the end of the current data sheet. Override it if
   * needed
   */
  virtual void endSheet() override{
    // qDebug() << "endSheet";
  };

  /**
   * callback that indicates a new line start. Override it if needed.
   */

  virtual void
  startLine() override
  {

    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() ";
    if(m_sample == "20120906_balliau_extract_1_A01_urnb-1")
      {
        m_rt1.addPeptideAsSeamark(m_peptideId, m_scan - 1);
      }
    if(m_sample == "20120906_balliau_extract_1_A02_urzb-1")
      {
        m_rt2.addPeptideAsSeamark(m_peptideId, m_scan - 1);
      }

    m_peptideId  = "";
    m_sample     = "";
    m_scan       = 0;
    m_sequenceLi = "";

    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() ";
  };

  /**
   * callback that indicates a line ending. Override it if needed.
   */

  virtual void
  endLine() override
  {
    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__
             << "() _is_title=" << _is_title;
    m_col = 0;
    if(!_is_title)
      {
      }
    _is_title = false;
    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__
             << "() _is_title=" << _is_title;
  };

  /**
   * callback that report the content of the current cell in a dedicated Cell
   * object. Override it if you need to retrieve cell content.
   */
  virtual void
  setCell(const OdsCell &cell) override
  {

    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() " << m_col;
    if(_is_title)
      return;
    m_col++;
    // Peptide ID,sample,scan,Sequence (top)
    if(m_col == 1)
      {
        if(!cell.isString())
          return;
        m_peptideId = cell.getStringValue();
      }
    if(m_col == 2)
      {
        m_sample = cell.getStringValue();
      }
    if(m_col == 3)
      {
        m_scan = (std::size_t)cell.getDoubleValue();
      }
    if(m_col == 4)
      {
        m_sequenceLi = cell.getStringValue();
      }

    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() ";
  };

  /**
   * callback that report the end of the ODS document. Override it if you need
   * to know that reading is finished.
   */
  virtual void
  endDocument()
  {
    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() ";
  };

  private:
  bool _is_title = true;
  std::size_t m_col;
  pappso::MsRunRetentionTime<QString> &m_rt1;
  pappso::MsRunRetentionTime<QString> &m_rt2;

  QString m_peptideId;
  QString m_sample;
  std::size_t m_scan;
  QString m_sequenceLi;
};


int
main(int argc, char *argv[])
{
  // QCoreApplication a(argc, argv);

  qDebug() << "init test MSrun alignment";
  cout << endl << "..:: Test MSrun alignment ::.." << endl;
  QTime timer;

#if USEPAPPSOTREE == 1
  cout << endl << "..:: Test MSrun alignment starts ::.." << endl;
  pappso::MsFileAccessor file_access_A01(
    "/gorgone/pappso/data_extraction_pappso/mzXML/"
    //"/home/langella/data1/mzxml/"
    "20120906_balliau_extract_1_A01_urnb-1.mzXML",
    "runa1");

  pappso::MsRunReaderSPtr msrunA01 =
    file_access_A01.getMsRunReaderSPtrByRunId("", "runa01");


  cout << endl
       << "..:: reading 20120906_balliau_extract_1_A01_urnb ::.." << endl;
  pappso::MsRunRetentionTime<QString> rt_msrunA01(msrunA01);


  pappso::MsFileAccessor file_access_A02(
    "/gorgone/pappso/data_extraction_pappso/mzXML/"
    //"/home/langella/data1/mzxml/"
    "20120906_balliau_extract_1_A02_urzb-1.mzXML",
    "runa1");

  pappso::MsRunReaderSPtr msrunA02 =
    file_access_A02.getMsRunReaderSPtrByRunId("", "runa02");


  cout << endl
       << "..:: reading 20120906_balliau_extract_1_A02_urzb ::.." << endl;
  pappso::MsRunRetentionTime<QString> rt_msrunA02(msrunA02);
  rt_msrunA02.setMs2MedianFilter(pappso::FilterMorphoMedian(10));
  rt_msrunA02.setMs2MeanFilter(pappso::FilterMorphoMean(15));
  rt_msrunA02.setMs1MeanFilter(pappso::FilterMorphoMean(1));

  cout << endl << "..:: reading peptide table ::.." << endl;


  QFile realfile(
    QString(CMAKE_SOURCE_DIR).append("/test/data/alignment/peptide_table.ods"));
  CustomHandler handler_rt(rt_msrunA01, rt_msrunA02);
  OdsDocReader realreader_prm(handler_rt);
  // realreader_prm.setSeparator(TsvSeparator::tab);
  try
    {
      realreader_prm.parse(&realfile);
    }
  catch(OdsException &error)
    {
      cerr << endl << "ERROR: " << error.qwhat().toStdString() << endl;
    }
  realfile.close();

  rt_msrunA01.computePeptideRetentionTimes();

  cout << endl
       << "rt_msrunA01 seamarks " << rt_msrunA01.getSeamarks().size() << endl;


  rt_msrunA02.computePeptideRetentionTimes();


  cout << endl
       << "rt_msrunA02 seamarks " << rt_msrunA02.getSeamarks().size() << endl;


  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() "
           << rt_msrunA01.getSeamarks()[0].entityHash << " "
           << rt_msrunA01.getSeamarks()[0].retentionTime << " | "
           << rt_msrunA02.getSeamarks()[0].entityHash << " "
           << rt_msrunA02.getSeamarks()[0].retentionTime;

  pappso::Trace rt_align;
  try
    {
      rt_align = rt_msrunA02.align(rt_msrunA01);
    }
  catch(pappso::PappsoException &error)
    {
      cerr << endl << "ERROR: " << error.qwhat().toStdString() << endl;
    }


  pappso::Trace common_delta_rt =
    rt_msrunA02.getCommonDeltaRt(rt_msrunA01.getSeamarks());


  writeTrace("delta_rt.ods", common_delta_rt);
  writeTrace("rt_align.ods", rt_align);
  cout << endl
       << "rt_msrunA02 seamarks in common with rt_msrunA01 "
       << common_delta_rt.size() << endl;
  if(common_delta_rt.size() != 4251)
    {
      cerr << "common_delta_rt.size() " << common_delta_rt.size() << " != 4251"
           << endl;

      // the same as in MassChroQ 2.2.15
      return 1;
    }


  for(auto rt : rt_align)
    {
      // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() " <<
      // rt.x
      //         << " " << rt.y;
    }

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "() first="
           << rt_msrunA01.getMs1RetentionTimeVector().front().retentionTime
           << " last="
           << rt_msrunA01.getMs1RetentionTimeVector().back().retentionTime;
  cerr << "corrected values " << rt_msrunA02.getNumberOfCorrectedValues() << " "
       << endl;
#endif
  return 0;
}
