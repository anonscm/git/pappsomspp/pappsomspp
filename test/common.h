#pragma once

#include <QDebug>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/pappsoexception.h>
#include <iostream>
#include <iomanip>

using namespace pappso;

MassSpectrum
readMgf(const QString &filename)
{
  try
    {
      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
      MsFileAccessor accessor(filename, "msrun");
      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";
      MsRunReaderSPtr reader =
        accessor.msRunReaderSp(accessor.getMsRunIds().front());
      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
               << accessor.getMsRunIds().front().get()->getXmlId();
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
                << reader->spectrumListSize() << std::endl;
      MassSpectrumSPtr spectrum_sp = reader->massSpectrumSPtr(0);
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
                << spectrum_sp.get()->size() << std::endl;
      spectrum_sp.get()->debugPrintValues();
      spectrum_sp.get()->sortMz();
      return *(spectrum_sp.get());
    }
  catch(pappso::PappsoException &error)
    {
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                << QString("ERROR reading file %1 : %2")
                     .arg(filename)
                     .arg(error.qwhat())
                     .toStdString()
                     .c_str();
      throw error;
    }
}
