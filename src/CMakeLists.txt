# CMake script for PAPPSOms++ library
# Author: Olivier Langella
# Created: 03/03/2015
# Rework: April 2020 (Coronavirus confinement)

message(\n${BoldRed}"Now configuring src/ for ${PROJECT}"${ColourReset}\n)


configure_file (${CMAKE_SOURCE_DIR}/src/pappsomspp/config.h.cmake ${CMAKE_SOURCE_DIR}/src/pappsomspp/config.h)


# custompwiz File list
set(PWIZ_CPP_FILES
	custompwiz/pwiz/utility/minimxml/XMLWriter.cpp
	custompwiz/pwiz/utility/minimxml/SAXParser.cpp
	custompwiz/pwiz/utility/misc/IterationListener.cpp
	custompwiz/pwiz/utility/misc/BinaryData.cpp
	custompwiz/pwiz/utility/misc/Base64.cpp
	custompwiz/pwiz/utility/misc/Filesystem.cpp
	custompwiz/pwiz/utility/misc/IntegerSet.cpp
	custompwiz/pwiz/utility/misc/random_access_compressed_ifstream.cpp
	custompwiz/pwiz/data/common/diff_std.cpp
	custompwiz/pwiz/data/msdata/Version.cpp
	custompwiz/pwiz/utility/misc/SHA1Calculator.cpp
	custompwiz/pwiz/data/msdata/Serializer_MGF.cpp
	custompwiz/pwiz/data/msdata/Serializer_MSn.cpp
	custompwiz/pwiz/data/msdata/IO.cpp
	custompwiz/pwiz/utility/misc/SHA1.cpp
	custompwiz/pwiz/data/msdata/References.cpp
	custompwiz/pwiz/data/msdata/SpectrumList_BTDX.cpp
	custompwiz/pwiz/data/msdata/ChromatogramList_mzML.cpp
	custompwiz/pwiz/data/msdata/SpectrumList_mzML.cpp
	custompwiz/pwiz/data/msdata/MSDataFile.cpp
	custompwiz/pwiz/data/msdata/BinaryDataEncoder.cpp
	custompwiz/pwiz/data/msdata/SpectrumList_mzXML.cpp
	custompwiz/pwiz/data/msdata/MSData.cpp
	custompwiz/pwiz/data/msdata/MSNumpress.cpp
	custompwiz/pwiz/data/msdata/LegacyAdapter.cpp
	custompwiz/pwiz/data/msdata/Reader.cpp
	custompwiz/pwiz/data/msdata/SpectrumList_MSn.cpp
	custompwiz/pwiz/data/msdata/Serializer_mzML.cpp
	custompwiz/pwiz/data/msdata/Index_mzML.cpp
	custompwiz/pwiz/data/msdata/SpectrumWorkerThreads.cpp
	custompwiz/pwiz/data/msdata/SpectrumList_MGF.cpp
	custompwiz/pwiz/data/msdata/DefaultReaderList.cpp
	custompwiz/pwiz/data/msdata/Serializer_mzXML.cpp
	custompwiz/pwiz/data/common/ParamTypes.cpp
	custompwiz/pwiz/data/common/cv.cpp
	custompwiz/pwiz/data/common/CVTranslator.cpp
	)

add_definitions(-DWITHOUT_MZ5)


########################################################
# Files
set(CPP_FILES
	pappsomspp/utils.cpp
	pappsomspp/mzrange.cpp
	pappsomspp/precision.cpp

	pappsomspp/processing/combiners/tracecombiner.cpp
	pappsomspp/processing/combiners/traceminuscombiner.cpp
	pappsomspp/processing/combiners/tracepluscombiner.cpp

	pappsomspp/processing/filters/filterlocalmaximum.cpp
	pappsomspp/processing/filters/filtermorpho.cpp
	pappsomspp/processing/filters/filterpass.cpp
	pappsomspp/processing/filters/filterpseudocentroid.cpp
	pappsomspp/processing/filters/filterresample.cpp
	pappsomspp/processing/filters/filtersuite.cpp
	pappsomspp/processing/filters/filtertandemremovec13.cpp
	pappsomspp/processing/filters/filtertriangle.cpp
	pappsomspp/processing/filters/savgolfilter.cpp

	pappsomspp/processing/filters/pwiz/ms/peakpickerqtof.hpp
	pappsomspp/processing/filters/pwiz/ms/simplepicker.hpp
	pappsomspp/processing/filters/pwiz/resample/convert2dense.hpp

	pappsomspp/processing/tandemwrapper/tandemwrapperrun.cpp
	pappsomspp/processing/tandemwrapper/xtandeminputsaxhandler.cpp
	pappsomspp/processing/tandemwrapper/xtandemoutputsaxhandler.cpp
	pappsomspp/processing/tandemwrapper/xtandempresetsaxhandler.cpp

	pappsomspp/massspectrum/massspectrumid.cpp
	pappsomspp/massspectrum/massspectrum.cpp
	pappsomspp/massspectrum/qualifiedmassspectrum.cpp

	pappsomspp/processing/combiners/massdatacombinerinterface.cpp
	pappsomspp/processing/combiners/massspectrumcombiner.cpp
	pappsomspp/processing/combiners/massspectrumpluscombiner.cpp
	pappsomspp/processing/combiners/massspectrumminuscombiner.cpp

	pappsomspp/amino_acid/aabase.cpp
	pappsomspp/amino_acid/aa.cpp
	pappsomspp/amino_acid/aamodification.cpp

	pappsomspp/fasta/fastafileindexer.cpp
	pappsomspp/fasta/fastareader.cpp
	pappsomspp/fasta/fastaoutputstream.cpp

	pappsomspp/grouping/grpexperiment.cpp
	pappsomspp/grouping/grppeptide.cpp
	pappsomspp/grouping/grpprotein.cpp
	pappsomspp/grouping/grpgroup.cpp
	pappsomspp/grouping/grpsubgroup.cpp
	pappsomspp/grouping/grppeptideset.cpp
	pappsomspp/grouping/grpmappeptidetogroup.cpp
	pappsomspp/grouping/grpmappeptidetosubgroupset.cpp
	pappsomspp/grouping/grpsubgroupset.cpp
	pappsomspp/grouping/grpgroupingmonitor.cpp

	pappsomspp/msfile/msfileaccessor.cpp
	pappsomspp/msfile/msfilereader.cpp
	pappsomspp/msfile/pwizmsfilereader.cpp
	pappsomspp/msfile/timsmsfilereader.cpp
	pappsomspp/msfile/xymsfilereader.cpp

	pappsomspp/msrun/alignment/msrunretentiontime.cpp
	pappsomspp/msrun/alignment/template.cpp
	pappsomspp/msrun/msrundatasettreenode.cpp
	pappsomspp/msrun/msrundatasettree.cpp
	pappsomspp/msrun/msrunid.cpp
	pappsomspp/msrun/msrunreader.cpp
	pappsomspp/msrun/mzxmloutput.cpp
	pappsomspp/msrun/private/pwizmsrunreader.cpp
	pappsomspp/msrun/private/timsmsrunreader.cpp
	pappsomspp/msrun/private/timsmsrunreaderms2.cpp
	pappsomspp/msrun/xymsrunreader.cpp

	pappsomspp/obo/obopsimod.cpp
	pappsomspp/obo/filterobopsimodsink.cpp
	pappsomspp/obo/filterobopsimodtermaccession.cpp
	pappsomspp/obo/filterobopsimodtermlabel.cpp
	pappsomspp/obo/filterobopsimodtermdiffmono.cpp
	pappsomspp/obo/filterobopsimodtermname.cpp

	pappsomspp/peptide/ion.cpp
	pappsomspp/peptide/peptideinterface.cpp
	pappsomspp/peptide/peptide.cpp
	pappsomspp/peptide/peptidefragment.cpp
	pappsomspp/peptide/peptidefragmention.cpp
	pappsomspp/peptide/peptidefragmentionlistbase.cpp
	pappsomspp/peptide/peptidenaturalisotope.cpp
	pappsomspp/peptide/peptidenaturalisotopelist.cpp
	pappsomspp/peptide/peptidenaturalisotopeaverage.cpp
	pappsomspp/peptide/peptiderawfragmentmasses.cpp
	pappsomspp/peptide/peptidestrparser.cpp

	pappsomspp/protein/protein.cpp
	pappsomspp/protein/enzyme.cpp
	pappsomspp/protein/peptidesizefilter.cpp
	pappsomspp/protein/peptidebuilder.cpp
	pappsomspp/protein/peptidevariablemodificationbuilder.cpp
	pappsomspp/protein/peptidevariablemodificationreplacement.cpp
	pappsomspp/protein/peptidefixedmodificationbuilder.cpp
	pappsomspp/protein/peptidemodificatorbase.cpp
	pappsomspp/protein/peptidemodificatortee.cpp
	pappsomspp/protein/peptidemethioninremove.cpp
	pappsomspp/protein/peptidemodificatorpipeline.cpp
	pappsomspp/protein/peptidesemienzyme.cpp

	pappsomspp/psm/experimental/ionisotoperatioscore.cpp
	pappsomspp/psm/morpheus/morpheusscore.cpp
	pappsomspp/psm/peakionmatch.cpp
	pappsomspp/psm/peakionisotopematch.cpp
	pappsomspp/psm/peptidespectrummatch.cpp
	pappsomspp/psm/peptideisotopespectrummatch.cpp
	pappsomspp/psm/xtandem/xtandemhyperscore.cpp
	pappsomspp/psm/xtandem/xtandemhyperscorebis.cpp
	pappsomspp/psm/xtandem/xtandemspectrumprocess.cpp

	pappsomspp/trace/datapoint.cpp
	pappsomspp/trace/linearregression.cpp
	pappsomspp/trace/maptrace.cpp
	pappsomspp/trace/trace.cpp

	pappsomspp/vendors/tims/xicextractor/timsdirectxicextractor.cpp
	pappsomspp/vendors/tims/xicextractor/timsxicextractorinterface.cpp
	pappsomspp/vendors/tims/timsbindec.cpp
	pappsomspp/vendors/tims/timsdata.cpp
	pappsomspp/vendors/tims/timsframe.cpp
	pappsomspp/vendors/tims/timsframebase.cpp
	pappsomspp/vendors/tims/timsms2centroidfilter.cpp

	pappsomspp/xic/xic.cpp
	pappsomspp/xic/qualifiedxic.cpp
	pappsomspp/xic/xicpeptideinterface.cpp
	pappsomspp/xic/xicpeptidefragmention.cpp
	pappsomspp/xic/xicpeptidefragmentionnaturalisotope.cpp

	pappsomspp/processing/detection/tracedetectionzivy.cpp
	pappsomspp/processing/detection/tracedetectionmoulon.cpp
	pappsomspp/processing/detection/tracepeak.cpp

	pappsomspp/xicextractor/private/msrunslice.cpp
	pappsomspp/xicextractor/private/msrunxicextractor.cpp
	pappsomspp/xicextractor/private/msrunxicextractordisk.cpp
	pappsomspp/xicextractor/private/msrunxicextractordiskbuffer.cpp
	pappsomspp/xicextractor/msrunxicextractorfactory.cpp
	pappsomspp/xicextractor/msrunxicextractorinterface.cpp
	)


find_package(Qt5 COMPONENTS Core Gui Svg Xml PrintSupport Sql Concurrent REQUIRED)

message("Qt5Core_LIBRARIES ${Qt5Core_LIBRARIES}")
message("QT_CORE_LIB ${QT_CORE_LIB}")

set(pappsomspp_RCCS libpappsomsppresources.qrc)
# generate rules for building source files from the resources
qt5_add_resources(pappsomspp_RCC_SRCS ${pappsomspp_RCCS})

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

### This is required because now CMake only automocs
### the generated files and not the source files.
# NOT sure about this. Let's comment that for the moment.
# set_source_files_properties(${pappsomspp_RCC_SRCS} PROPERTIES SKIP_AUTOMOC ON)


#############################################################
# Build the static lib
add_library(pappsomspp-static STATIC ${PWIZ_CPP_FILES} ${CPP_FILES} ${pappsomspp_RCC_SRCS})

set_target_properties(pappsomspp-static
	PROPERTIES OUTPUT_NAME pappsomspp
	CLEAN_DIRECT_OUTPUT 1
	)

target_include_directories(pappsomspp-static PUBLIC
	${CMAKE_SOURCE_DIR}/src/custompwiz
	${CMAKE_SOURCE_DIR}/src/custompwiz/libraries/boost_aux
	)

target_link_libraries(pappsomspp-static 
	Qt5::Core
	Qt5::Gui
	Qt5::Xml
	Qt5::Svg
	Qt5::PrintSupport
	Qt5::Sql
	Qt5::Concurrent
	Zstd::Zstd
	Alglib::Alglib
	SQLite::SQLite3
	Boost::iostreams
	Boost::filesystem
	Boost::thread
	Boost::chrono
	ZLIB::ZLIB
	)


# FIXME: should check that this still required. It was years ago.
if(WIN64)

	target_link_libraries(pappsomspp-static 
		stdc++
		)
endif()


#############################################################
# Build the shared lib
add_library(pappsomspp-shared SHARED ${PWIZ_CPP_FILES} ${CPP_FILES} ${pappsomspp_RCC_SRCS})

set_target_properties(pappsomspp-shared
	PROPERTIES OUTPUT_NAME pappsomspp
	CLEAN_DIRECT_OUTPUT 1
	VERSION ${PAPPSOMSPP_VERSION}
	SOVERSION ${PAPPSOMSPP_VERSION_MAJOR}
	)

target_include_directories(pappsomspp-shared PUBLIC
	${CMAKE_SOURCE_DIR}/src/custompwiz
	${CMAKE_SOURCE_DIR}/src/custompwiz/libraries/boost_aux
	)


target_link_libraries(pappsomspp-shared 
	Qt5::Core
	Qt5::Gui
	Qt5::Xml
	Qt5::Svg
	Qt5::PrintSupport
	Qt5::Sql
	Qt5::Concurrent
	Zstd::Zstd
	SQLite::SQLite3
	Alglib::Alglib
	Boost::iostreams
	Boost::filesystem
	Boost::thread
	Boost::chrono
	ZLIB::ZLIB
	)


# Should check that this still required. It was years ago.
if(WIN64)

	target_link_libraries(pappsomspp-shared 
		stdc++
		)

endif()


if(MINGW)
else(MINGW)
	install(TARGETS pappsomspp-shared LIBRARY DESTINATION lib${LIB_SUFFIX})
endif(MINGW)

install(TARGETS pappsomspp-static DESTINATION lib${LIB_SUFFIX})


# Install headers
install(DIRECTORY pappsomspp/ DESTINATION include/pappsomspp FILES_MATCHING PATTERN "*.h")


#IF(NOT PAPPSOMSPPWIDGET)
add_subdirectory("pappsomspp/widget")
#ENDIF(NOT PAPPSOMSPPWIDGET)
