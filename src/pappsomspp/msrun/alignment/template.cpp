#include "msrunretentiontime.h"
#include "msrunretentiontime.cpp"
#include "../../peptide/peptide.h"

template class pappso::MsRunRetentionTime<QString>;
template class pappso::MsRunRetentionTime<const pappso::Peptide *>;
