
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <QDir>
#include <QDebug>
#include "obopsimod.h"
#include "../pappsoexception.h"
#include <iostream>

inline void
initMyResource()
{
  Q_INIT_RESOURCE(libpappsomsppresources);
}


namespace pappso
{


QRegExp OboPsiModTerm::m_firstParse("^([a-z,A-Z]+):\\s(.*)$");
QRegExp OboPsiModTerm::m_findExactPsiModLabel(
  "^(.*)\\sEXACT\\sPSI-MOD-label\\s\\[\\]$");

// synonym: "Carbamidomethyl" RELATED PSI-MS-label []
QRegExp OboPsiModTerm::m_findRelatedPsiMsLabel(
  "^(.*)\\sRELATED\\sPSI-MS-label\\s\\[\\]$");

void
OboPsiModTerm::parseLine(const QString &line)
{
  // qDebug() << "OboPsiModTerm::parseLine begin " << line;
  // id: MOD:00007
  if(m_firstParse.exactMatch(line))
    {
      QStringList pline = m_firstParse.capturedTexts();
      // qDebug() << "OboPsiModTerm::parseLine match " << pline[0] << pline[1];
      if(pline[1] == "id")
        {
          m_accession = pline[2].trimmed();
          // qDebug() << "OboPsiModTerm::parseLine accession = " << m_accession;
        }
      else if(pline[1] == "name")
        {
          m_name = pline[2].trimmed();
          // qDebug() << "OboPsiModTerm::parseLine accession = " << m_accession;
        }
      else if(pline[1] == "xref")
        {
          // xref: DiffMono: "1.007276"
          if(m_firstParse.exactMatch(pline[2]))
            {
              QStringList psecond = m_firstParse.capturedTexts();
              if(psecond[1] == "DiffMono")
                {
                  m_diffMono = psecond[2].replace("\"", "").toDouble();
                  // qDebug() << "OboPsiModTerm::parseLine m_diffMono = " <<
                  // m_diffMono;
                }
              else if(psecond[1] == "DiffFormula")
                {
                  m_diffFormula = psecond[2].trimmed().replace("\"", "");
                  // qDebug() << "OboPsiModTerm::parseLine m_diffFormula = |" <<
                  // m_diffFormula<<"|";
                }
              else if(psecond[1] == "Origin")
                {
                  m_origin =
                    psecond[2].trimmed().replace("\"", "").replace(",", "");
                  // qDebug() << "OboPsiModTerm::parseLine m_diffFormula = |" <<
                  // m_diffFormula<<"|";
                }
            }
        }
      else if(pline[1] == "synonym")
        {
          // synonym: "Se(S)Res" EXACT PSI-MOD-label []
          if(m_findExactPsiModLabel.exactMatch(pline[2]))
            {
              m_psiModLabel =
                m_findExactPsiModLabel.capturedTexts()[1].trimmed().replace(
                  "\"", "");
              // qDebug() << "OboPsiModTerm::parseLine m_psiModLabel = |" <<
              // m_psiModLabel<<"|";
            }
          else if(m_findRelatedPsiMsLabel.exactMatch(pline[2]))
            {
              m_psiMsLabel =
                m_findRelatedPsiMsLabel.capturedTexts()[1].trimmed().replace(
                  "\"", "");
              // qDebug() << "OboPsiModTerm::parseLine m_psiModLabel = |" <<
              // m_psiModLabel<<"|";
            }
        }
    }
}
void
OboPsiModTerm::clearTerm()
{
  m_accession   = "";
  m_name        = "";
  m_psiModLabel = "";
  m_diffFormula = "";
  m_diffMono    = 0;
  m_origin      = "";
}

OboPsiMod::OboPsiMod(OboPsiModHandlerInterface &handler) : m_handler(handler)
{
  qDebug() << "OboPsiMod::OboPsiMod";
  initMyResource();
  parse();
}

OboPsiMod::~OboPsiMod()
{
}


void
OboPsiMod::parse()
{
  // std::cout << "OboPsiMod::parse Begin parsing OBO file" << endl;
  qDebug() << "OboPsiMod::parse Begin parsing OBO file";
  QFile obofile(":/resources/PSI-MOD.obo");
  if(!obofile.exists())
    {
      throw PappsoException(
        QObject::tr("PSI-MOD OBO resource file : %1 not found")
          .arg(obofile.fileName()));
    }
  obofile.open(QIODevice::ReadOnly);
  QTextStream p_in(&obofile);

  // Search accession conta
  // QTextStream in(p_in);
  QString line = p_in.readLine();
  bool in_term = false;
  while(!p_in.atEnd())
    {
      // qDebug() << "OboPsiMod::parse line "<< line;
      if(line.startsWith("[Term]"))
        {
          in_term = true;
          m_term.clearTerm();
        }
      else if(line.isEmpty())
        {
          if(in_term)
            {
              m_handler.setOboPsiModTerm(m_term);
              in_term = false;
            }
        }
      else
        {
          if(in_term)
            m_term.parseLine(line);
          // m_handler.setSequence(line);
        }
      line = p_in.readLine();
    }
  if(in_term)
    {
      m_handler.setOboPsiModTerm(m_term);
    }
  // p_in->close();

  obofile.close();
}

} // namespace pappso
