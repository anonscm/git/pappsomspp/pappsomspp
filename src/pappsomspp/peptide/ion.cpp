/**
 * \file pappsomspp/peptide/ion.cpp
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief  ion interface
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ion.h"
#include "../pappsoexception.h"

namespace pappso
{
Ion::Ion()
{
}

Ion::~Ion()
{
}

pappso_double
Ion::getMz(unsigned int charge) const
{
  if(charge == 0)
    {
      throw PappsoException("charge == 0");
    }
  return ((getMass() + (MHPLUS * charge)) / charge);
}
} // namespace pappso
