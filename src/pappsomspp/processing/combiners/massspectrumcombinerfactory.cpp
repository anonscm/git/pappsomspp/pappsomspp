/**
 * \file pappsomspp/mass_range.cpp
 * \date 4/3/2015
 * \author Olivier Langella
 * \brief object to handle a mass range (an mz value + or - some delta)
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "precision.h"
#include "mzrange.h"
#include "exception/exceptionnotpossible.h"
#include <QStringList>
#include <cmath>
#include <QDebug>

namespace pappso
{


PrecisionFactory::MapDaltonPrecision PrecisionFactory::m_mapDalton = [] {
  MapDaltonPrecision ret;

  return ret;
}();


PrecisionFactory::MapPpmPrecision PrecisionFactory::m_mapPpm = [] {
  MapPpmPrecision ret;

  return ret;
}();


PrecisionFactory::MapResPrecision PrecisionFactory::m_mapRes = [] {
  MapResPrecision ret;

  return ret;
}();


PrecisionFactory::MapMzPrecision PrecisionFactory::m_mapMz = [] {
  MapMzPrecision ret;

  return ret;
}();


pappso_double
PrecisionBase::getNominal() const
{
  return m_nominal;
}


PrecisionPtr
PrecisionFactory::fromString(const QString &str)
{

  // The format of the string is <number><space *><string> with string either
  // "ppm" or "dalton" or "res" or "mz".
  //
  // If there only once component, that is, <string> is omitted and charge is
  // not provided, then "dalton" is considered.

  QStringList list = str.split(QRegExp("\\s+"), QString::SkipEmptyParts);

  if(list.size() > 0)
    {
      bool ok;
      pappso_double value = list[0].toDouble(&ok);
      if(!ok)
        {
          throw ExceptionNotPossible(
            QObject::tr("ERROR getting precision from string :\nunable to "
                        "convert %1 to number in %2")
              .arg(value)
              .arg(str));
        }
      if(list.size() == 1)
        {
          return PrecisionFactory::getDaltonInstance(value);
        }
      else
        {
          if(list.size() == 2)
            {
              if(list[1].toLower() == "dalton")
                {
                  return PrecisionFactory::getDaltonInstance(value);
                }

              if(list[1].toLower() == "ppm")
                {
                  return PrecisionFactory::getPpmInstance(value);
                }

              if(list[1].toLower() == "res")
                {
                  return PrecisionFactory::getResInstance(value);
                }

              throw ExceptionNotPossible(
                QObject::tr("ERROR getting precision from string :\nprecision "
                            "unit %1 to not known in  %2")
                  .arg(list[1])
                  .arg(str));
            }
          else if(list.size() == 3)
            {
              // We are handling a mz precision instance.
              // Thus, first item is the value, the second item has to be
              // charge.

              bool ok;
              int charge = list[1].toInt(&ok);

              if(!ok || list[2] != "mz")
                {
                  throw ExceptionNotPossible(
                    QObject::tr("ERROR getting charge from string :\nunable to "
                                "convert %1 to int in %2, or string is not mz")
                      .arg(charge)
                      .arg(str));
                }

              return PrecisionFactory::getMzInstance(value, charge);
            }
        }
    }

  throw ExceptionNotPossible(QObject::tr("ERROR getting precision from string "
                                         ":\nunable to convert %1 to precision")
                               .arg(str));
}

PrecisionPtr
PrecisionFactory::getDaltonInstance(pappso_double value)
{
  MapDaltonPrecision::iterator it = m_mapDalton.find(value);
  if(it == m_mapDalton.end())
    {
      // not found
      std::pair<MapDaltonPrecision::iterator, bool> insert_res =
        m_mapDalton.insert(std::pair<pappso_double, DaltonPrecision *>(
          value, new DaltonPrecision(value)));
      it = insert_res.first;
    }
  else
    {
      // found
    }
  return it->second;
}


PrecisionPtr
PrecisionFactory::getPpmInstance(pappso_double value)
{
  if(!value)
    throw ExceptionNotPossible(
      QObject::tr("Fatal error at %1@%2 in %3 -- %4(). ")
        .arg(__FILE__)
        .arg(__LINE__)
        .arg(__FUNCTION__)
        .arg("ERROR trying to set a Resolution precision value of 0."
             "Program aborted."));

  MapPpmPrecision::iterator it = m_mapPpm.find(value);

  if(it == m_mapPpm.end())
    {
      // Not found.
      std::pair<MapPpmPrecision::iterator, bool> insert_res =
        m_mapPpm.insert(std::pair<pappso_double, PpmPrecision *>(
          value, new PpmPrecision(value)));
      it = insert_res.first;
    }
  else
    {
      // found
    }
  return it->second;
}


PrecisionPtr
PrecisionFactory::getResInstance(pappso_double value)
{
  if(!value)
    throw ExceptionNotPossible(
      QObject::tr("Fatal error at %1@%2 in %3 -- %4(). ")
        .arg(__FILE__)
        .arg(__LINE__)
        .arg(__FUNCTION__)
        .arg("ERROR trying to set a Resolution precision value of 0."
             "Program aborted."));

  MapResPrecision::iterator it = m_mapRes.find(value);

  if(it == m_mapRes.end())
    {
      // not found
      std::pair<MapResPrecision::iterator, bool> insert_res =
        m_mapRes.insert(std::pair<pappso_double, ResPrecision *>(
          value, new ResPrecision(value)));
      it = insert_res.first;
    }
  else
    {
      // found
    }
  return it->second;
}


PrecisionPtr
PrecisionFactory::getMzInstance(pappso_double value, int charge)
{
  // Special find_if search because we need to confirm the presence of the
  // value, charge pair, not only the value.

  MapMzPrecision::iterator it = std::find_if(
    std::begin(m_mapMz),
    std::end(m_mapMz),
    [value, charge](const std::pair<pappso_double, MzPrecision *> &pair) {
      return ((pair.first == value) && (pair.second->charge() == charge));
    });

  if(it == m_mapMz.end())
    {
      // Not found.
      std::pair<MapMzPrecision::iterator, bool> insert_res =
        m_mapMz.insert(std::pair<pappso_double, MzPrecision *>(
          value, new MzPrecision(value, charge)));

      it = insert_res.first;
    }
  else
    {
      // Found.
    }

  return it->second;
}


DaltonPrecision::DaltonPrecision(pappso_double x) : PrecisionBase(x)
{
}

DaltonPrecision::~DaltonPrecision()
{
}

PrecisionUnit
DaltonPrecision::unit() const
{
  return PrecisionUnit::dalton;
}

pappso_double
DaltonPrecision::delta(pappso_double value) const
{
  return m_nominal;
}

QString
DaltonPrecision::toString() const
{
  return (QString("%1 dalton").arg(m_nominal));
}


PpmPrecision::PpmPrecision(pappso_double x) : PrecisionBase(x)
{
}


PpmPrecision::~PpmPrecision()
{
}

PrecisionUnit
PpmPrecision::unit() const
{
  return PrecisionUnit::ppm;
}


pappso_double
PpmPrecision::delta(pappso_double value) const
{
  return ((value / ONEMILLION) * m_nominal);
}


QString
PpmPrecision::toString() const
{
  return (QString("%1 ppm").arg(m_nominal));
}


ResPrecision::ResPrecision(pappso_double x) : PrecisionBase(x)
{
}


ResPrecision::~ResPrecision()
{
}

PrecisionUnit
ResPrecision::unit() const
{
  return PrecisionUnit::res;
}


pappso_double
ResPrecision::delta(pappso_double value) const
{
  return (value / m_nominal);
}


QString
ResPrecision::toString() const
{
  return (QString("%1 res").arg(m_nominal));
}


MzPrecision::MzPrecision(pappso_double value, int charge)
  : PrecisionBase(value), m_charge(charge)
{
}


MzPrecision::~MzPrecision()
{
}


PrecisionUnit
MzPrecision::unit() const
{
  return PrecisionUnit::mz;
}


pappso_double
MzPrecision::delta(pappso_double value) const
{
  if(!m_charge)
    return m_nominal;
  else
    return m_nominal / m_charge;
}


int
MzPrecision::charge() const
{
  return m_charge;
};


QString
MzPrecision::toString() const
{
  return (QString("%1 %2 mz").arg(m_nominal).arg(m_charge));
};

} // namespace pappso
