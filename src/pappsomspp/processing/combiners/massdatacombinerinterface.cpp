
/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "massdatacombinerinterface.h"


namespace pappso
{


MassDataCombinerInterface::MassDataCombinerInterface(int decimal_places)
  : m_decimalPlaces(decimal_places)
{
}


MassDataCombinerInterface::~MassDataCombinerInterface()
{
}

void
MassDataCombinerInterface::setDecimalPlaces(int value)
{
  m_decimalPlaces = value;
}


int
MassDataCombinerInterface::getDecimalPlaces() const
{
  return m_decimalPlaces;
}


void
MassDataCombinerInterface::setFilterResampleKeepXRange(
  const FilterResampleKeepXRange &range)
{
  m_filterXRange        = range;
  m_isApplyXRangeFilter = true;
}


MapTrace &
MassDataCombinerInterface::combine(MapTrace &map_trace,
                                   Iterator begin,
                                   Iterator end)
{
  for(Iterator iterator = begin; iterator != end; ++iterator)
    combine(map_trace, *(*iterator));

  return map_trace;
}


} // namespace pappso
