
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "grppeptide.h"
#include "../utils.h"

using namespace pappso;

GrpPeptide::GrpPeptide(QString sequence, pappso_double mass)
  : m_sequence(sequence.replace("L", "I")), m_mass(mass)
{
}

GrpPeptide::~GrpPeptide()
{
}

bool
GrpPeptide::operator<(const GrpPeptide &other) const
{
  if(m_sequence == other.m_sequence)
    {
      return (m_mass < other.m_mass);
    }
  else
    {
      return (m_sequence < other.m_sequence);
    }
}

void
GrpPeptide::setRank(unsigned int i)
{
  m_rank = i;
}
void
GrpPeptide::setGroupNumber(unsigned int i)
{
  m_groupNumber = i;
}

unsigned int
GrpPeptide::getGroupNumber() const
{
  return m_groupNumber;
}
unsigned int
GrpPeptide::getRank() const
{
  return m_rank;
}
const QString &
GrpPeptide::getSequence() const
{
  return m_sequence;
}
const QString
GrpPeptide::getGroupingId() const
{
  return QString("pep%1%2")
    .arg(Utils::getLexicalOrderedString(m_groupNumber))
    .arg(Utils::getLexicalOrderedString(m_rank));
}
