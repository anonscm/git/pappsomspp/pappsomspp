
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "grpprotein.h"


#include "grpexperiment.h"
#include "grppeptide.h"
#include "../utils.h"

using namespace pappso;
GrpProtein::GrpProtein(const QString &accession, const QString &description)
  : m_accession(accession.simplified()), m_description(description.simplified())
{
}


GrpProtein::GrpProtein(const GrpProtein &other)
  : m_accession(other.m_accession),
    m_description(other.m_description),
    m_grpPeptidePtrList(other.m_grpPeptidePtrList)
{
}

GrpProtein::~GrpProtein()
{
}

unsigned int
GrpProtein::getCount() const
{
  return m_count;
}
void
GrpProtein::countPlus()
{
  m_count++;
}
void
GrpProtein::setSubGroupNumber(unsigned int i)
{
  m_subGroupNumber = i;
}

unsigned int
GrpProtein::getGroupNumber() const
{
  return m_groupNumber;
}

unsigned int
GrpProtein::getSubGroupNumber() const
{
  return m_subGroupNumber;
}
unsigned int
GrpProtein::getRank() const
{
  return m_rank;
}
const QString
GrpProtein::getGroupingId() const
{
  if(m_groupNumber == 0)
    {
      return "";
    }
  return QString("%1.%2.%3")
    .arg(Utils::getLexicalOrderedString(m_groupNumber))
    .arg(Utils::getLexicalOrderedString(m_subGroupNumber))
    .arg(Utils::getLexicalOrderedString(m_rank));
}
bool
GrpProtein::operator==(const GrpProtein &other) const
{
  return (m_accession == other.m_accession);
}


const QString &
GrpProtein::getAccession() const
{
  return m_accession;
}
const QString &
GrpProtein::getDescription() const
{
  return m_description;
}
void
GrpProtein::push_back(GrpPeptide *p_grpPeptide)
{
  // p_grpPeptide->push_back(this);
  m_grpPeptidePtrList.push_back(p_grpPeptide);
}

void
GrpProtein::strip()
{
  qDebug() << "GrpProtein::strip begin " << this->m_accession;
  // m_grpPeptidePtrList.sort();
  std::sort(m_grpPeptidePtrList.begin(), m_grpPeptidePtrList.end());
  // m_grpPeptidePtrList.unique();
  m_grpPeptidePtrList.erase(
    std::unique(m_grpPeptidePtrList.begin(), m_grpPeptidePtrList.end()),
    m_grpPeptidePtrList.end());
  qDebug() << "GrpProtein::strip end";
}

void
GrpProtein::setRank(unsigned int i)
{
  m_rank = i;
}
void
GrpProtein::setGroupNumber(unsigned int i)
{
  m_groupNumber = i;
}
