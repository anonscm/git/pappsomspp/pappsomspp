/**
 * \file pappsomspp/xicextractor/msrunxicextractorfactory.cpp
 * \date 07/05/2018
 * \author Olivier Langella
 * \brief factory to build XIC extractor on an MsRun file
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "msrunxicextractorfactory.h"
#include "private/msrunxicextractor.h"
#include "private/msrunxicextractordisk.h"
#include "private/msrunxicextractordiskbuffer.h"

namespace pappso
{


MsRunXicExtractorFactory MsRunXicExtractorFactory::m_instance =
  MsRunXicExtractorFactory();

MsRunXicExtractorFactory &
MsRunXicExtractorFactory::getInstance()
{
  return m_instance;
}


MsRunXicExtractorFactory::MsRunXicExtractorFactory()
{
}
MsRunXicExtractorFactory::~MsRunXicExtractorFactory()
{
}

void
MsRunXicExtractorFactory::setTmpDir(const QString &dir_name)
{
  m_tmpDirName = dir_name;
}
MsRunXicExtractorInterfaceSp
MsRunXicExtractorFactory::buildMsRunXicExtractorSp(
  MsRunReaderSPtr &msrun_reader) const
{
  std::shared_ptr<MsRunXicExtractor> msrun_xic_extractor_sp =
    std::make_shared<MsRunXicExtractor>(MsRunXicExtractor(msrun_reader));
  return (msrun_xic_extractor_sp);
}

MsRunXicExtractorInterfaceSp
MsRunXicExtractorFactory::buildMsRunXicExtractorDiskSp(
  MsRunReaderSPtr &msrun_reader) const
{
  std::shared_ptr<MsRunXicExtractorDisk> msrun_xic_extractor_sp =
    std::make_shared<MsRunXicExtractorDisk>(
      MsRunXicExtractorDisk(msrun_reader, QDir(m_tmpDirName)));

  msrun_xic_extractor_sp.get()->prepareExtractor();
  return (msrun_xic_extractor_sp);
}

MsRunXicExtractorInterfaceSp
MsRunXicExtractorFactory::buildMsRunXicExtractorDiskBufferSp(
  MsRunReaderSPtr &msrun_reader) const
{
  std::shared_ptr<MsRunXicExtractorDiskBuffer> msrun_xic_extractor_sp =
    std::make_shared<MsRunXicExtractorDiskBuffer>(
      MsRunXicExtractorDiskBuffer(msrun_reader, QDir(m_tmpDirName)));
  msrun_xic_extractor_sp.get()->prepareExtractor();
  return (msrun_xic_extractor_sp);
}

} // namespace pappso
