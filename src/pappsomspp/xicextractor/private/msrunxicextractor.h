/**
 * \file pappsomspp/xicextractor/private/msrunxicextractorpwiz.h
 * \date 07/05/2018
 * \author Olivier Langella
 * \brief simple proteowizard based XIC extractor
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../msrunxicextractorinterface.h"

class MsRunXicExtractorFactory;

namespace pappso
{


class MsRunXicExtractor : public MsRunXicExtractorInterface
{
  friend MsRunXicExtractorFactory;

  protected:
  std::vector<MsRunXicExtractorPoints> m_msrun_points;


  MsRunXicExtractor(MsRunReaderSPtr &msrun_reader);

  virtual void
  getXicFromPwizMSDataFile(std::vector<Xic *> &xic_list,
                           const std::vector<MzRange> &mass_range_list,
                           pappso::pappso_double rt_begin,
                           pappso::pappso_double rt_end);

  public:
  MsRunXicExtractor(const MsRunXicExtractor &other);
  virtual ~MsRunXicExtractor();
  virtual XicCstSPtr getXicCstSPtr(const MzRange &mz_range,
                                   pappso::pappso_double rt_begin,
                                   pappso::pappso_double rt_end) override;

  virtual std::vector<XicCstSPtr>
  getXicCstSPtrList(const std::vector<MzRange> &mz_range_list) override;
};


} // namespace pappso
