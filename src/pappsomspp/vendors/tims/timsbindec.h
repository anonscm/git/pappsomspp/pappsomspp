/**
 * \file pappsomspp/vendors/tims/timsbindec.h
 * \date 23/08/2019
 * \author Olivier Langella
 * \brief binary file handler of Bruker's TimsTof raw data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QFile>
#include <QFileInfo>
#include "timsframe.h"

namespace pappso
{

/**
 * @todo write docs
 */
class TimsBinDec
{
  public:
  /**
   * Default constructor
   */
  TimsBinDec(const QFileInfo &timsBinFile, int timsCompressionType);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TimsBinDec(const TimsBinDec &other);

  /**
   * Destructor
   */
  ~TimsBinDec();

  // TimsFrameCstSPtr getTimsFrameCstSPtr(std::size_t timsId);

  TimsFrameSPtr getTimsFrameSPtrByOffset(std::size_t timsId,
                                         std::size_t timsOffset) const;

  private:
  void indexingFile();

  private:
  QString m_timsBinFile;
  // QMutex m_mutex;
  // std::vector<quint64> m_indexArray;
};
} // namespace pappso
