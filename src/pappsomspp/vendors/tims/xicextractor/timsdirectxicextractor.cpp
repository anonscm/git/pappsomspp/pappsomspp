/**
 * \file pappsomspp/vendors/tims/xicextractor/timsdirectxicextractor.cpp
 * \date 21/09/2019
 * \author Olivier Langella
 * \brief minimum functions to extract XICs from Tims Data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "timsdirectxicextractor.h"

using namespace pappso;

TimsDirectXicExtractor::TimsDirectXicExtractor(TimsData *mp_tims_data)
  : pappso::TimsXicExtractorInterface(mp_tims_data)
{
}

TimsDirectXicExtractor::~TimsDirectXicExtractor()
{
}


void
TimsDirectXicExtractor::extractTimsXicList(
  std::vector<TimsXicStructure> &timsXicList, double rtRange) const
{
  if(timsXicList.size() == 0)
    return;
  std::sort(timsXicList.begin(),
            timsXicList.end(),
            [](const TimsXicStructure &a, const TimsXicStructure &b) {
              return a.rtTarget < b.rtTarget;
            });

  std::vector<std::size_t> tims_frameid_list =
    mp_timsData->getTimsMS1FrameIdRange(timsXicList[0].rtTarget - rtRange,
                                        timsXicList.back().rtTarget + rtRange);

  std::vector<TimsXicStructure>::iterator itXicListbegin = timsXicList.begin();
  std::vector<TimsXicStructure>::iterator itXicListend   = timsXicList.begin();

  for(std::size_t frame_id : tims_frameid_list)
    {
      TimsFrameCstSPtr frame_sptr = mp_timsData->getTimsFrameCstSPtr(frame_id);
      double rt                   = frame_sptr.get()->getTime();
      while((itXicListbegin != timsXicList.end()) &&
            ((itXicListbegin->rtTarget - rtRange) < rt))
        {
          itXicListbegin++;
        }
      while((itXicListend != timsXicList.end()) &&
            (rt < (itXicListend->rtTarget + rtRange)))
        {
          itXicListend++;
        }
      frame_sptr.get()->extractTimsXicListInRtRange(
        itXicListbegin, itXicListend, m_xicExtractMethod);
    }
}
