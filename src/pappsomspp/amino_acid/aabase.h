/**
 * \file pappsomspp/amino_acid/aabase.h
 * \date 7/3/2015
 * \author Olivier Langella
 * \brief private amino acid model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <map>

#include "../types.h"
#include "atomnumberinterface.h"

namespace pappso
{

class AaBase : public AtomNumberInterface
{

  public:
  virtual pappso_double getMass() const;
  virtual const char &getLetter() const;

  virtual void replaceLeucineIsoleucine();

  static const std::vector<AminoAcidChar> &getAminoAcidCharList();


  protected:
  AaBase(char aa_letter);
  AaBase(AminoAcidChar aa_char);
  AaBase(const AaBase &aabase);
  virtual ~AaBase();

  virtual int getNumberOfAtom(AtomIsotopeSurvey atom) const override;
  int getNumberOfIsotope(Isotope isotope) const override;
  static pappso_double getAaMass(char aa_letter);

  protected:
  char m_aaLetter;

  private:
  using AaMassMap         = std::map<char, pappso_double>;
  using AaIntMap          = std::map<char, unsigned int>;
  using AminoAcidCharList = std::vector<AminoAcidChar>;

  static AaMassMap m_aaMassMap;
  static AaIntMap m_aaNumberOfCarbonMap;
  static AaIntMap m_aaNumberOfSulfurMap;
  static AaIntMap m_aaNumberOfHydrogenMap;
  static AaIntMap m_aaNumberOfNitrogenMap;
  static AaIntMap m_aaNumberOfOxygenMap;
  static AminoAcidCharList m_aminoAcidCharList;
};

} /* namespace pappso */
